package com.pear2pear.car2car.assets

import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class Car(
    var brand: String,
    var model: String,
    var plates: String,
    var position: String,
    var registrationDate : String,
    var username: String ="polo",
    var sharingPeriodStart: String? = null,
    var sharingPeriodEnd: String? = null,
    var using: Boolean? = null,
    var carID: String = "777") {
    /*fun toJSON():JSONObject{
        var out=JSONObject()
        val coo = JSONArray(position.split("-").map { it.toDouble() })
        val posobj = JSONObject().put("coordinates",coo).put("type","Point")
        out.put("produttore",brand)
        out.put("modello",model)
        out.put("targa",plates)
        out.put("posizione",posobj)
        out.put("dataIm",JSONObject().put("\$date",SimpleDateFormat("yyyy-MM-dd", Locale.ITALY).parse(registrationDate).time))
        out.put("inuso",using)
        out.put("proprietarioID",username)
        if(!sharingPeriodStart.equals(null)){
            val obj = JSONObject().put("inizio",JSONObject().put("\$date",SimpleDateFormat("yyyy-MM-dd", Locale.ITALY).parse(sharingPeriodStart).time))
                .put("fine",JSONObject().put("\$date",SimpleDateFormat("yyyy-MM-dd", Locale.ITALY).parse(sharingPeriodEnd).time))
            out.put("sharing",obj)

        }
        out.put("_id",JSONObject().put("\$oid",carID))
        return out
    }*/

}