package com.pear2pear.car2car.assets

import kotlin.math.floor

class User(
    var user:String,
    var exp:Int=987,
    var percMission1:Int=0,
    var percMission2:Int=0,
    var percMission3:Int=0

) {
    fun getLevel():Int{
        return (floor(exp/100.toDouble()) +1).toInt()  //100 punti esp per ogni livello
    }

    fun getcurrentExp():Int{
        return exp%100
    }

    fun getremainingExp():Int{
        return 100-(exp%100)
    }
}