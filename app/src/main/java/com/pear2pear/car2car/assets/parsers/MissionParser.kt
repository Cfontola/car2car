package com.pear2pear.car2car.assets.parsers

import com.pear2pear.car2car.assets.Mission
import org.json.JSONObject

class MissionParser(var missionjson: JSONObject) {
    constructor(mission: Mission):this(parseJSON(mission))

    companion object{
        private fun parseJSON(mission:Mission): JSONObject {
            val out= JSONObject()
            out.put("titolo", mission.title)
            out.put("descrizione", mission.description)
            out.put("stato", mission.status)
            // TODO: id missione?
            return out
        }
    }

    fun toJSON(): JSONObject {
        return missionjson
    }

    fun toMission(): Mission{
        val obj = missionjson
        val title = obj.getString("titolo")
        val description = obj.getString("descrizione")
        val status = obj.getInt("stato")

        return Mission(title,description,status)
    }
}