package com.pear2pear.car2car.assets

class Mission (
    var title: String,
    var description: String,
    var status: Int) {
}