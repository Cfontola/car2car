package com.pear2pear.car2car.assets.parsers

import com.pear2pear.car2car.assets.Rent
import org.json.JSONObject
import java.util.*

class RentParser(var rentjson: JSONObject) {
    constructor(rent: Rent):this(parseJSON(rent))

    companion object{
        private fun parseJSON(rent: Rent): JSONObject{
            val out = JSONObject()
            out.put("author",rent.User)
            out.put("addressedto",rent.toUser)
            out.put("date",JSONObject().put("\$date",rent.date.time))
            out.put("carId",rent.IdCar)
            out.put("isAccepted",rent.confirm)
            out.put("isEnded",rent.isEnd)
            //out.put("_id",JSONObject().put("\$oid",ID))
            return out
        }
    }

    fun toJSON(): JSONObject{
        return rentjson
    }

    fun toRent(): Rent{
        val obj = rentjson
        val date = Date(obj.getJSONObject("date").getLong("\$date"))
        val oid = obj.getJSONObject("_id").getString("\$oid")
        return Rent(
                    obj.getString("author"), obj.getString("addressedto"), date, obj.getBoolean("isAccepted"),
                    obj.getBoolean("isEnded"), obj.getString("carId"),oid
                )
    }
}