package com.pear2pear.car2car.assets.parsers

import com.pear2pear.car2car.assets.User
import org.json.JSONObject

class UserParser(var userjson: JSONObject) {
    constructor(user:User):this(parseJSON(user))

    companion object{
        private fun parseJSON(u:User): JSONObject {
            val out = JSONObject()
            out.put("user",u.user)
            out.put("exp",u.exp)
            return out
        }
    }

    fun toJSON(): JSONObject {
        return userjson
    }

    fun toUser():User{
        return User(userjson.getString("user"),userjson.getInt("exp"))
    }
}