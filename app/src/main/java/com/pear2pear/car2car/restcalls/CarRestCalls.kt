package com.pear2pear.car2car.restcalls
import com.pear2pear.car2car.assets.Car
import com.pear2pear.car2car.assets.parsers.CarParser
import com.pear2pear.car2car.restcalls.httpservices.HTTPRequest
import com.pear2pear.car2car.restcalls.resources.cars.ActiveDateCarsResource
import com.pear2pear.car2car.restcalls.resources.cars.UserCarsResource
import org.json.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class CarRestCalls {
    companion object{

        fun insertCar(name:String,c: Car):Boolean{
            val parsed = CarParser(c).toJSON()
            return HTTPRequest(UserCarsResource(name)).executepost(parsed).getBoolean("executed")
        }

        fun getCarsByName(name:String):ArrayList<Car>{
            val jarray = HTTPRequest(UserCarsResource(name)).executeget().getJSONArray("data")
            val out = arrayListOf<Car>()
            for (i in 0 until jarray.length()){
                out.add(CarParser(jarray[i] as JSONObject).toCar())
            }
            return out
        }

        fun getActiveByDateExcluding(date:Date,x:Double,y:Double,name:String=""):ArrayList<Car>{
            val jarray = HTTPRequest(ActiveDateCarsResource(date,x,y)).executeget().getJSONArray("data")
            val out = arrayListOf<Car>()
            for (i in 0 until jarray.length()){
                out.add(CarParser(jarray[i] as JSONObject).toCar())
            }
            return out.filter { car -> car.username!=name  } as ArrayList<Car>
        }

        // temp
        /*fun executeget(resourceUrl:String):JSONObject{
            //System.err.println(resourceUrl)
            val url=URL(resourceUrl)
            val connection=url.openConnection() as HttpURLConnection
            connection.requestMethod="GET"

            if(connection.responseCode!=200){
                throw IOException(String.format("Failed Connection - error %s",connection.responseCode))
            }

            val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
            var response=""
            readbuffer.use {
                response=readbuffer.readLines().joinToString(separator = "")
            }
            return JSONObject(response)
        }

        fun executepost(resourceUrl:String,formargs:JSONObject):Boolean{
            val url=URL(resourceUrl)
            val connection=url.openConnection() as HttpURLConnection
            connection.requestMethod="POST"
            connection.setRequestProperty("Content-Type","application/json; utf-18")
            connection.doOutput=true
            val outstream = connection.outputStream
            outstream.use {
                val message = formargs.toString().toByteArray()
                outstream.write(message)
            }

            val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
            var response=""
            readbuffer.use {
                response=readbuffer.readLines().joinToString(separator = "")
            }
            return JSONObject(response).getBoolean("executed")
        }*/
    }
}