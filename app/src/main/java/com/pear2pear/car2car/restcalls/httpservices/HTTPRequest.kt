package com.pear2pear.car2car.restcalls.httpservices

import com.pear2pear.car2car.restcalls.resources.Resource
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class HTTPRequest(var resource: Resource) {
    fun executeget(): JSONObject {
        val url = URL(resource.getURL())
        val connection=url.openConnection() as HttpURLConnection
        connection.requestMethod="GET"

        if(connection.responseCode!=200){
            throw IOException(String.format("Failed Connection - error %s",connection.responseCode))
        }

        val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
        var response=""
        readbuffer.use {
            response=readbuffer.readLines().joinToString(separator = "")
        }
        return JSONObject(response)
    }

    fun executepost(arg: JSONObject): JSONObject {
        val url = URL(resource.getURL())
        val connection=url.openConnection() as HttpURLConnection
        connection.requestMethod="POST"

        connection.setRequestProperty("Content-Type","application/json; utf-18")
        connection.doOutput=true
        val outstream = connection.outputStream
        outstream.use {
            val message = arg.toString().toByteArray()
            outstream.write(message)
        }

        if(connection.responseCode!=201){
            throw IOException(String.format("Failed Connection - error %s",connection.responseCode))
        }

        val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
        var response=""
        readbuffer.use {
            response=readbuffer.readLines().joinToString(separator = "")
        }
        return JSONObject(response)
    }

    fun executeput(arg: JSONObject = JSONObject()): JSONObject {
        val url = URL(resource.getURL())
        val connection=url.openConnection() as HttpURLConnection
        connection.requestMethod="PUT"

        connection.setRequestProperty("Content-Type","application/json; utf-18")
        connection.doOutput=true
        val outstream = connection.outputStream
        outstream.use {
            val message = arg.toString().toByteArray()
            outstream.write(message)
        }

        if(connection.responseCode!=200 && connection.responseCode!=201){
            throw IOException(String.format("Failed Connection - error %s",connection.responseCode))
        }

        val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
        var response=""
        readbuffer.use {
            response=readbuffer.readLines().joinToString(separator = "")
        }
        return JSONObject(response)
    }

    fun executedelete(arg: JSONObject = JSONObject()): JSONObject {
        val url = URL(resource.getURL())
        val connection=url.openConnection() as HttpURLConnection
        connection.requestMethod="DELETE"

        connection.setRequestProperty("Content-Type","application/json; utf-18")
        connection.doOutput=true
        val outstream = connection.outputStream
        outstream.use {
            val message = arg.toString().toByteArray()
            outstream.write(message)
        }

        if(connection.responseCode!=200 && connection.responseCode!=201){
            throw IOException(String.format("Failed Connection - error %s",connection.responseCode))
        }

        val readbuffer = BufferedReader(InputStreamReader(connection.inputStream))
        var response=""
        readbuffer.use {
            response=readbuffer.readLines().joinToString(separator = "")
        }
        return JSONObject(response)
    }
}