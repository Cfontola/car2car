package com.pear2pear.car2car.restcalls.resources

interface Resource {
    companion object {
        const val baseAddress: String = "http://207.154.207.31"
    }
    fun getURL():String
}