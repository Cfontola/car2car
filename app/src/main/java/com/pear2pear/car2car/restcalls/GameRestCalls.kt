package com.pear2pear.car2car.restcalls

import com.pear2pear.car2car.assets.User
import com.pear2pear.car2car.assets.parsers.UserParser
import com.pear2pear.car2car.restcalls.httpservices.HTTPRequest
import com.pear2pear.car2car.restcalls.resources.gamification.GlobalStatsResource
import com.pear2pear.car2car.restcalls.resources.gamification.UserStatsResource
import org.json.JSONObject

class GameRestCalls {
    companion object{

        fun getUserGame(user:String): User {
            val u= HTTPRequest(UserStatsResource(user)).executeget().getJSONObject("data")
            return UserParser(u).toUser()
        }

        fun getLeaderBoard(): ArrayList<User> {
            var data = HTTPRequest(GlobalStatsResource()).executeget().getJSONArray("data")
            val out = arrayListOf<User>()
            for (i in 0 until data.length()){
                out.add(UserParser(data[i] as JSONObject).toUser())
            }
            return out
        }

        /*fun parseUser(o: JSONObject): User {
            val obj=o.getJSONObject("data")
            return com.pear2pear.car2car.assets.User(obj.getString("user"), obj.getInt("exp"))
        }*/
    }
}