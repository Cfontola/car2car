package com.pear2pear.car2car.restcalls.resources.cars

import com.pear2pear.car2car.restcalls.resources.Resource
import java.text.SimpleDateFormat
import java.util.*

class ActiveDateCarsResource(var dfrom: Date, var x: Double, var y: Double):Resource {
    companion object {
        val dateparser = SimpleDateFormat("yyyy-MM-dd", Locale.ITALY)
    }
    override fun getURL(): String {
        val parseddate = dateparser.format(dfrom)
        return Resource.baseAddress + "/cars/activebydate/$parseddate/$x/$y"
    }
}