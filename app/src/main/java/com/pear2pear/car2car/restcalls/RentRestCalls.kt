package com.pear2pear.car2car.restcalls

import com.pear2pear.car2car.assets.Rent
import com.pear2pear.car2car.assets.parsers.RentParser
import com.pear2pear.car2car.restcalls.httpservices.HTTPRequest
import com.pear2pear.car2car.restcalls.resources.rents.ReceivedRentsResource
import com.pear2pear.car2car.restcalls.resources.rents.SentRentsResource
import com.pear2pear.car2car.restcalls.resources.rents.SpecificReceivedRentResource
import com.pear2pear.car2car.restcalls.resources.rents.SpecificSentRentResource
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class RentRestCalls {
    companion object {
        /*fun parseRent(j: JSONObject): ArrayList<com.pear2pear.car2car.assets.Rent> {
            val out = ArrayList<com.pear2pear.car2car.assets.Rent>()
            val array = j.getJSONArray("data")
            for (i in 0 until array.length()) {
                val obj = array[i] as JSONObject
                val date = Date(obj.getJSONObject("date").getLong("\$date"))
                val oid = obj.getJSONObject("_id").getString("\$oid")
                out.add(
                    com.pear2pear.car2car.assets.Rent(
                        obj.getString("author"), obj.getString("addressedto"), date, obj.getBoolean("isAccepted"),
                        obj.getBoolean("isEnded"), obj.getString("carId"), oid
                    )
                )
            }
            return out
        }*/

        fun getMyRents(name: String): ArrayList<Rent> {
            val jarray = HTTPRequest(SentRentsResource(name)).executeget().getJSONArray("data")
            val out = arrayListOf<Rent>()
            for (i in 0 until jarray.length()) {
                val obj = jarray[i] as JSONObject
                out.add(RentParser(obj).toRent())
            }
            return out
        }

        fun getReceivedRents(name: String): ArrayList<Rent> {
            val jarray = HTTPRequest(ReceivedRentsResource(name)).executeget().getJSONArray("data")
            val out = arrayListOf<Rent>()
            for (i in 0 until jarray.length()) {
                val obj = jarray[i] as JSONObject
                out.add(RentParser(obj).toRent())
            }
            return out
        }

        fun insertRentRequest(r: Rent): Boolean {
            return HTTPRequest(SentRentsResource(r.User)).executepost(RentParser(r).toJSON()).getBoolean("executed")
        }

        fun acceptRent(Rentid: String, name:String): Boolean{

            val res = HTTPRequest(SpecificReceivedRentResource(name, Rentid)).executeput()
            return res.getBoolean("executed")
        }

        fun confirmKeys(Rentid: String, name: String): Boolean {
            return HTTPRequest(SpecificSentRentResource(name,Rentid)).executeput().getBoolean("executed")
        }

        fun abortRent(Rentid: String, name: String): Boolean {
            return HTTPRequest(SpecificSentRentResource(name, Rentid)).executedelete().getBoolean("executed")
        }

        fun denyRent(Rentid: String, name: String): Boolean {
            return HTTPRequest(SpecificReceivedRentResource(name, Rentid)).executedelete(JSONObject()
                .put("command", "deny")).getBoolean("executed")
        }

        fun closeRent(Rentid: String, name: String): Boolean{
            return HTTPRequest(SpecificReceivedRentResource(name, Rentid)).executedelete(JSONObject()
                .put("command", "close")).getBoolean("executed")
        }
    }
}