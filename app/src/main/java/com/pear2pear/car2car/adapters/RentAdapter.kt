
package com.pear2pear.car2car.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pear2pear.car2car.R

class RentAdapter(val context: Context, val notifications: List<com.pear2pear.car2car.assets.Rent>, val notificationItemClick: (com.pear2pear.car2car.assets.Rent) -> Unit): RecyclerView.Adapter<RentAdapter.NotificationHolder>() {

    override fun onBindViewHolder(RentHolder: NotificationHolder, position: Int) {
        RentHolder.bindRent(notifications[position],context)
    }

    override fun getItemCount(): Int {
        return notifications.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_single_rent,parent,false)
        return NotificationHolder(view, notificationItemClick)
    }

    inner class NotificationHolder(itemView:View?, val itemClick: (com.pear2pear.car2car.assets.Rent) -> Unit): RecyclerView.ViewHolder(itemView) {
        // val nIcon = itemView?.findViewById<ImageView>(com.pear2pear.car2car.R.id.iconItemRent)
        val nInfo  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.textItemRent)

        fun bindRent(rent: com.pear2pear.car2car.assets.Rent, context: Context){
            // nIcon = TODO
            if(!rent.confirm) {
                nInfo?.text = "Hai richiesto l'auto a ${rent.toUser} \nIn data  ${rent.date.date}/${rent.date.month+1}/${rent.date.year+1900} \nIn attesa di conferma... "
            } else {
                nInfo?.text = "${rent.toUser} ha accettato la tua richiesta \nPer la data: ${rent.date.date}/${rent.date.month+1}/${rent.date.year+1900}\nIn attesa di scambio chiavi... "
            }
            itemView.setOnClickListener { itemClick(rent) }
        }
    }

}