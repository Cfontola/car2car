
package com.pear2pear.car2car.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pear2pear.car2car.R

class CarAdapter(val context: Context, val cars: List<com.pear2pear.car2car.assets.Car>, val carItemClick: (com.pear2pear.car2car.assets.Car) -> Unit): RecyclerView.Adapter<CarAdapter.CarHolder>() {

    override fun onBindViewHolder(holder: CarHolder, position: Int) {
        holder.bindCar(cars[position],context)
    }

    override fun getItemCount(): Int {
        return cars.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_single_car,parent,false)
        return CarHolder(view, carItemClick)
    }

    inner class CarHolder(itemView:View?, val itemClick: (com.pear2pear.car2car.assets.Car) -> Unit): RecyclerView.ViewHolder(itemView) {
        val cBrand = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carBrand)
        val cModel = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carModel)
        val cPlates = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carPlates)
        val cPosition = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carPosition)
        val cRegistrationDate  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carRegistrationDate)
        val cUsername  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carUsername)
        val cSharingPeriodStart  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carSharingPeriodStart)
        val cSharingPeriodEnd  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carSharingPeriodEnd)
        val cUsing  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carUsing)
        val cID  = itemView?.findViewById<TextView>(com.pear2pear.car2car.R.id.carID)

        fun bindCar(car: com.pear2pear.car2car.assets.Car, context: Context){
            cBrand?.text = car.brand
            cModel?.text = car.model
            cPlates?.text = car.plates
            cPosition?.text = car.position
            cRegistrationDate?.text = car.registrationDate
            cUsername?.text = car.username
            cSharingPeriodStart?.text = car.sharingPeriodStart
            cSharingPeriodEnd?.text = car.sharingPeriodEnd
            cUsing?.text = car.using.toString()
            cID?.text = car.carID

            itemView.setOnClickListener { itemClick(car) }
        }
    }

}