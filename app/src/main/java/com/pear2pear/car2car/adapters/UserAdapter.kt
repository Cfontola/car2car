package com.pear2pear.car2car.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pear2pear.car2car.R
import com.pear2pear.car2car.assets.User

class UserAdapter(val context: Context, val leaderboard: List<User>, val userItemClick: (User) -> Unit): RecyclerView.Adapter<UserAdapter.LeaderboardHolder>() {

    override fun onBindViewHolder(UserHolder: LeaderboardHolder, position: Int) {
        UserHolder.bindUser(leaderboard[position],position.toString(),context)
    }

    override fun getItemCount(): Int {
        return leaderboard.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderboardHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_single_user,parent,false)
        return LeaderboardHolder(view, userItemClick)
    }

    inner class LeaderboardHolder(itemView: View?, val itemClick: (User) -> Unit): RecyclerView.ViewHolder(itemView) {
        val uPosition = itemView?.findViewById<TextView>(R.id.textUserPosition)
        // uIcon = TODO
        val uName  = itemView?.findViewById<TextView>(R.id.textUserName)
        val uLevel = itemView?.findViewById<TextView>(R.id.textUserLevel)
        val uPe = itemView?.findViewById<TextView>(R.id.textUserPe)

        fun bindUser(user: User, position: String,context: Context){
            uPosition?.text = position
            uName?.text = user.user
            uLevel?.text = user.getLevel().toString()
            uPe?.text = user.exp.toString()
            itemView.setOnClickListener { itemClick(user) }
        }
    }

}