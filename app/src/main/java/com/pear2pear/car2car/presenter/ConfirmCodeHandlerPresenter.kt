package com.pear2pear.car2car.presenter

import android.content.Context
import android.widget.Toast
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.pear2pear.car2car.cognito.ConfirmCodeActivity
import com.pear2pear.car2car.model.ConfirmCodeHandlerModel
import java.lang.Exception


class ConfirmCodeHandlerPresenter(context: Context) :ConfirmCode{

    var confirmaodeactivity= context as ConfirmCodeActivity

    var model: ConfirmCodeHandlerModel= ConfirmCodeHandlerModel(context)



    val handler = object: GenericHandler {
        override fun onSuccess() {
            Toast.makeText(context, "Codice confermato corettamente, torna indietro", Toast.LENGTH_SHORT).show()
            confirmaodeactivity.finish()


        }

        override fun onFailure(exception: Exception?) {
            Toast.makeText(context, "Errore codice errato, riprova +${exception}", Toast.LENGTH_SHORT).show()

        }
    }


    override fun confirm(user:String,code:String){

       model.confirmUser(handler,user,code)



    }





}

