package com.pear2pear.car2car.presenter

import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler

interface ConfirmCodeHandler {
    fun confirmUser(handler: GenericHandler,user:String,code:String)
}