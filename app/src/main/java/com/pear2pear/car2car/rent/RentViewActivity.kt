package com.pear2pear.car2car.rent

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.pear2pear.car2car.R
import com.pear2pear.car2car.activities.HomeActivity
import com.pear2pear.car2car.restcalls.RentRestCalls
import kotlinx.android.synthetic.main.activity_rent_view.*
import java.lang.Exception

class RentViewActivity : AppCompatActivity() {

    private class CloseRentTask: AsyncTask<String, Int, Boolean>(){

        override fun doInBackground(vararg p0: String): Boolean {
            var out:Boolean
            println("vaDC "+ HomeActivity.User.current)
            try {
                out = RentRestCalls.closeRent(p0[0], HomeActivity.User.current)

            }
            catch(e: Exception){
                out = false
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: Boolean) {
            //super.onPostExecute(result)
            if (result!=null){


            }
            else{
                println("nonvaDC")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rent_view)
        if (intent.extras != null) {
            val bundle: Bundle = intent.extras
            textview_key.text = "scambio chiavi in data" + bundle.getInt("day") + "/" + bundle.getInt("month") + "/" +
                    bundle.getInt("year")
            start_rent_btn.text = bundle.getString("button")


        }
        start_rent_btn.setOnClickListener {

        }
    }
}
