package com.pear2pear.car2car.rent

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pear2pear.car2car.restcalls.RentRestCalls
import com.pear2pear.car2car.R
import com.pear2pear.car2car.activities.HomeActivity
import kotlinx.android.synthetic.main.activity_rent_edit.*
import java.lang.Exception

class RentEditActivity : AppCompatActivity() {

    var touser=""
    var day:Int=0
    var month:Int=0
    var year:Int=0
    var confirm=false
    var id=""
    var carid=""

    private class ConfirmRentTask: AsyncTask<String, Int, Boolean>(){

        override fun doInBackground(vararg p0: String): Boolean {
            var out:Boolean
            println("vaDC "+ HomeActivity.User.current)
            try {
                out = RentRestCalls.acceptRent(p0[0], HomeActivity.User.current)

            }
            catch(e: Exception){
                out = false
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: Boolean) {
            //super.onPostExecute(result)
            if (result!=null){


            }
            else{
                println("nonvaDC")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rent_edit)
        if( intent.extras != null ) {
            val bundle: Bundle = intent.extras
            touser = bundle.getString("touser")
            day = bundle.getInt("day")
            month = bundle.getInt("month")
            year = bundle.getInt("year")
            confirm = bundle.getBoolean("confirm")
            id = bundle.getString("id")
            carid=bundle.getString("carID")



            renttextView.text=touser + " ha richiesto la tua ${HomeActivity.CarFactory.returnCarByID(
                carid
            ).brand} per il ${day}/${month}/${year} confermi???"
        }

        confirmrentbutton.setOnClickListener(){
            println("DCDCDC "+id+" AAA "+ HomeActivity.User.current)
             ConfirmRentTask().execute(id)
            this.finish()
        }
    }


}
