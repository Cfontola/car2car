package com.pear2pear.car2car.fragments

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.android.gms.maps.*
import kotlinx.android.synthetic.main.fragment_search.*
import java.util.*

import android.widget.TextView
import com.google.android.gms.maps.model.*

import android.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.pear2pear.car2car.activities.CarViewActivity
import com.pear2pear.car2car.activities.HomeActivity
import com.pear2pear.car2car.R
import com.pear2pear.car2car.activities.CarEditActivity
import com.pear2pear.car2car.adapters.CarAdapter
import kotlinx.android.synthetic.main.fragment_my_cars.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [DashboardFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [DashboardFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class DashboardFragment : Fragment(), OnMapReadyCallback {

    private var listener: OnFragmentInteractionListener? = null
    lateinit var mapFragment : SupportMapFragment
    lateinit var googleMap : GoogleMap
    private var mMap: MapView? = null

    lateinit var adapter: CarAdapter

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mMap?.onSaveInstanceState(outState)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_search, container, false)
        val rootView = inflater.inflate(R.layout.fragment_search,container,false)
        /*mapFragment = supportFragmentManager.findFragmentById(R.id.maplayout) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it
        })*/



        //OnClickListener um Location zu speichern

        val btn1 = rootView.findViewById(R.id.buttonDate1) as Button
        btn1.setOnClickListener {
            showCalendar()
        }

/*
        val btn4 = rootView.findViewById(R.id.ButtonShowList) as Button
        btn4.setOnClickListener {
            showList()
            btn4.isClickable=false //LO POSSO PREMERE SOLO UNA VOLTA
        }
*/
        mMap = rootView?.findViewById(R.id.mapview) as MapView
        mMap?.onCreate(savedInstanceState)
        mMap?.getMapAsync(this)

        return rootView


    }

    // populate the views now that the layout has been inflated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("grafica - list di ${HomeActivity.CarFactory.cars.size} auto")
        // grafica dinamica
        adapter = CarAdapter(view.context, HomeActivity.CarFactory.cars) { car ->
            if(HomeActivity.User.current!="") {
                val carIntent = Intent(view.context, CarViewActivity::class.java)
                carIntent.putExtra("Brand", car.brand)
                carIntent.putExtra("Model", car.model)
                carIntent.putExtra("Targa", car.plates)
                carIntent.putExtra("Date", car.registrationDate)
                carIntent.putExtra("StartSh", car.sharingPeriodStart)
                carIntent.putExtra("EndSh", car.sharingPeriodEnd)
                carIntent.putExtra("Username", car.username)
                startActivity(carIntent)
            } else {
                Toast.makeText(activity, "Devi essere autenticato per poter prenotare un'auto", Toast.LENGTH_SHORT).show()
            }
        }
        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.setHasFixedSize(true)

    }


    override fun onResume() {
        super.onResume()
        mMap?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMap?.onPause()
    }

    override fun onStart() {
        super.onStart()
        mMap?.onStart()
    }

    override fun onStop() {
        super.onStop()
        mMap?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMap?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMap?.onLowMemory()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        //googleMap.addMarker(MarkerOptions().position(LatLng(45.406433,11.876761)).title("Marker"))
        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(45.406433,11.876761), 12.0f))
        if (HomeActivity.CarFactory.cars.size != 0) {
            for (i in 0 until HomeActivity.CarFactory.cars.size) {
                val title:String= HomeActivity.CarFactory.cars[i].brand + "/${i}"
                var markerx: MarkerOptions? = MarkerOptions()
                    .position(
                        LatLng(
                            HomeActivity.CarFactory.cars[i].position.split("-")[0].toDouble(),
                            HomeActivity.CarFactory.cars[i].position.split("-")[1].toDouble()
                        )
                    )
                    .title(title)
                    .snippet(HomeActivity.CarFactory.cars[i].model)
                //.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_foreground_garage))

                googleMap.addMarker(markerx)


            }
            googleMap.setOnInfoWindowClickListener(infoWindowClickListener)
            //googleMap.setOnMarkerClickListener(markerClickListener)


            //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(HomeActivity.CurrentPosition.Position, 14.5f))
            googleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(

                        HomeActivity.CurrentPosition.Position
                    , 15f
                )
            )
            googleMap.addCircle(
                CircleOptions()
                    .center(HomeActivity.CurrentPosition.Position)
                    .radius(500.0)
                    .fillColor(0x550055FF)
                    .strokeWidth(1f)
            )
        }
    }
    private val infoWindowClickListener=object:GoogleMap.OnInfoWindowClickListener{

        override fun onInfoWindowClick(p0: Marker?) {
            val i = p0!!.title.split("/")[1].toInt()
                openCarActivity(i)

        }
    }








    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DashboardFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DashboardFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    fun showCalendar(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)+1
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity,
            AlertDialog.THEME_DEVICE_DEFAULT_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            editFirstDate.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")


        }, year, month, day)

        dpd.show()
    }

    /*fun showClock(button:Int){
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)



        val tpd = TimePickerDialog(activity,AlertDialog.THEME_DEVICE_DEFAULT_DARK, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
            if(button==1){
                editFirstClock.setText(h.toString() + " : " + m )
            }

            else{
                editLastClock.setText(h.toString() + " : " + m )
            }

        }),hour,minute,false)

        tpd.show()
    }*/

    fun showList(){
        for( i in 0 until HomeActivity.CarFactory.cars.size ) {
            println(i.toString() + " dix " + HomeActivity.CarFactory.cars.size)

            val tv_dynamic = TextView(this.context)
            tv_dynamic.textSize = 20f
            tv_dynamic.setPadding(5,5,5,5)
            tv_dynamic.setBackgroundResource(R.color.colorPrimary)
            var x=i+1
            tv_dynamic.text ="$x "+ HomeActivity.CarFactory.returnCarInfo(i)
            tv_dynamic.setOnClickListener(){ // QUANDO PREMO IN UNA SINGOLA AUTO DELLA LISTA
                println(tv_dynamic.text)
                openCarActivity(i)
            }

            println( HomeActivity.CarFactory.returnCarInfo(i) )
            // add TextView to LinearLayout
            // CarSearchLayout.addView(tv_dynamic)
        }
    }

    fun openCarActivity(i:Int){
        if(HomeActivity.User.current!="") {
            val intent = Intent(activity, CarViewActivity::class.java)
            val brand = HomeActivity.CarFactory.cars[i].brand
            val model= HomeActivity.CarFactory.cars[i].model
            val date = HomeActivity.CarFactory.cars[i].registrationDate
            val targa = HomeActivity.CarFactory.cars[i].plates
            val startSh= HomeActivity.CarFactory.cars[i].sharingPeriodStart
            val endSh= HomeActivity.CarFactory.cars[i].sharingPeriodEnd
            val proprietario= HomeActivity.CarFactory.cars[i].username
            var id= HomeActivity.CarFactory.cars[i].carID
            intent.putExtra("Brand", brand)
            intent.putExtra("Model",model)
            intent.putExtra("Date", date)
            intent.putExtra("Targa", targa)
            intent.putExtra("StartSh",startSh)
            intent.putExtra("EndSh",endSh)
            intent.putExtra("Username",proprietario)
            intent.putExtra("Id",id)
            startActivity(intent)
        }
        else{
            Toast.makeText(activity, "Devi essere autenticato per poter prenotare un'auto", Toast.LENGTH_SHORT).show()
        }

    }
}







