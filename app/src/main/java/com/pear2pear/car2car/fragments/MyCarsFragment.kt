package com.pear2pear.car2car.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.AlarmClock
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.pear2pear.car2car.activities.CarEditActivity
import com.pear2pear.car2car.activities.CarInsertActivity
import com.pear2pear.car2car.activities.HomeActivity
import com.pear2pear.car2car.adapters.CarAdapter
import com.pear2pear.car2car.R
import kotlinx.android.synthetic.main.fragment_my_cars.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [myCarFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [myCarFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class myCarFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    lateinit var adapter: CarAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_my_cars, container, false)
        val rootView = inflater.inflate(R.layout.fragment_my_cars, container, false)

        val btn1 = rootView.findViewById(R.id.addautobtn) as Button
        btn1.setOnClickListener {
            val intent = Intent(activity, CarInsertActivity::class.java)
            val message = "DC"
            intent.putExtra(AlarmClock.EXTRA_MESSAGE, message)
            intent.putExtra("user", HomeActivity.User.current)
            startActivity(intent)
        }

        return rootView
    }

    // populate the views now that the layout has been inflated
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("grafica - list di ${HomeActivity.CarFactory.mycars.size} auto")
        // grafica dinamica
        adapter = CarAdapter(view.context, HomeActivity.CarFactory.mycars) { car ->
            val carIntent = Intent(view.context, CarEditActivity::class.java)
            carIntent.putExtra("Brand", car.brand)
            carIntent.putExtra("Model", car.model)
            carIntent.putExtra("Targa", car.plates)
            carIntent.putExtra("Date", car.registrationDate)
            carIntent.putExtra("StartSh", car.sharingPeriodStart)
            carIntent.putExtra("EndSh", car.sharingPeriodEnd)
            carIntent.putExtra("Username", car.username)
            startActivity(carIntent)
        }
        list_recycler_view.adapter = adapter

        list_recycler_view.layoutManager = LinearLayoutManager(view.context)
        list_recycler_view.setHasFixedSize(true)

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment myCarFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            myCarFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
