package com.pear2pear.car2car.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pear2pear.car2car.activities.HomeActivity
import com.pear2pear.car2car.rent.RentEditActivity
import com.pear2pear.car2car.rent.RentViewActivity
import com.pear2pear.car2car.R
import com.pear2pear.car2car.adapters.RentAdapter
import kotlinx.android.synthetic.main.fragment_notification.*


class NotificationFragment : Fragment() {


    lateinit var sentRentsAdapter: RentAdapter
    lateinit var recievedRentsAdapter: RentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_notification,container,false)
        /*
        val btn2 = rootView.findViewById(R.id.buttontest) as Button
        btn2.setOnClickListener {
          //var testRent= Rent(HomeActivity.User.current,"altro a caso", Date(118,11,1),false,false)
            btn2.isClickable=false
            for( i in 0 until HomeActivity.RentFactory.rents.size ) {
                val tv_dynamic = TextView(this.context)
                tv_dynamic.textSize = 20f
                tv_dynamic.setPadding(5, 5, 5, 5)
                tv_dynamic.setBackgroundResource(R.color.colorPrimary)
                tv_dynamic.text = HomeActivity.RentFactory.returnRentInfo(i)

                sentRentLayout.addView(tv_dynamic)
                tv_dynamic.setOnClickListener() {
                    if(HomeActivity.RentFactory.rents[i].confirm)
                    {
                    val rent = HomeActivity.RentFactory.rents[i]
                    val intent2=Intent(activity, RentViewActivity::class.java)
                    intent2.putExtra("day",rent.date.date)
                    intent2.putExtra("month",rent.date.month+1)
                    intent2.putExtra("year",rent.date.year+1900)
                    intent2.putExtra("button","prendi chiavi")
                    startActivity(intent2)
                    }
                }

            }

            for( i in 0 until HomeActivity.RentFactory.receiverent.size ){
                val tv_dynamic = TextView(this.context)
                tv_dynamic.textSize = 20f
                tv_dynamic.setPadding(5, 5, 5, 5)
                tv_dynamic.setBackgroundResource(R.color.colorPrimary)
                tv_dynamic.text = HomeActivity.RentFactory.returnreceiveRentInfo(i)
                println(tv_dynamic.text)
                ReceiveRentLayout.addView(tv_dynamic)
                tv_dynamic.setOnClickListener(){

                    val rent = HomeActivity.RentFactory.receiverent[i]
                    if(HomeActivity.RentFactory.receiverent[i].confirm){
                        val intent2=Intent(activity, RentViewActivity::class.java)
                        intent2.putExtra("day",rent.date.date)
                        intent2.putExtra("month",rent.date.month+1)
                        intent2.putExtra("year",rent.date.year+1900)
                        intent2.putExtra("button","cedi chiavi")
                        startActivity(intent2)
                    }
                    else{
                        val intent = Intent(activity, RentEditActivity::class.java)


                        intent.putExtra("touser", rent.User)
                        intent.putExtra("day",rent.date.date)
                        intent.putExtra("month",rent.date.month+1)
                        intent.putExtra("year",rent.date.year+1900)
                        intent.putExtra("confirm", rent.confirm)
                        intent.putExtra("id",rent.ID)
                        intent.putExtra("carID",rent.IdCar)

                        startActivity(intent)
                    }

                }

            }

        }
        */
        return rootView

        // return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    /* grafica dinamica recycleView */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // grafica dinamica noleggi ricevuti
        println("grafica - list di ${HomeActivity.RentFactory.receiverent.size} noleggi in entrata")
        recievedRentsAdapter = RentAdapter(view.context, HomeActivity.RentFactory.receiverent) { rent ->
            val recievedRentIntent = Intent(view.context, RentEditActivity::class.java)
            recievedRentIntent.putExtra("touser", rent.User)
            recievedRentIntent.putExtra("day",rent.date.date)
            recievedRentIntent.putExtra("month",rent.date.month+1)
            recievedRentIntent.putExtra("year",rent.date.year+1900)
            recievedRentIntent.putExtra("confirm", rent.confirm)
            recievedRentIntent.putExtra("id",rent.ID)
            recievedRentIntent.putExtra("carID",rent.IdCar)
            startActivity(recievedRentIntent)
        }
        recieved_rents_recycler_view.adapter = recievedRentsAdapter
        recieved_rents_recycler_view.layoutManager = LinearLayoutManager(view.context)
        recieved_rents_recycler_view.setHasFixedSize(true)

        // grafica dinamica noleggi inviati
        println("grafica - list di ${HomeActivity.RentFactory.rents.size} noleggi in uscita")
        sentRentsAdapter = RentAdapter(view.context, HomeActivity.RentFactory.rents) { rent ->
            val sentRentIntent = Intent(view.context, RentViewActivity::class.java)
            sentRentIntent.putExtra("day",rent.date.date)
            sentRentIntent.putExtra("month",rent.date.month+1)
            sentRentIntent.putExtra("year",rent.date.year+1900)
            sentRentIntent.putExtra("button","cedi chiavi")
            startActivity(sentRentIntent)
        }
        sent_rents_recycler_view.adapter = sentRentsAdapter
        sent_rents_recycler_view.layoutManager = LinearLayoutManager(view.context)
        sent_rents_recycler_view.setHasFixedSize(true)


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }


}
