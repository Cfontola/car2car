package com.pear2pear.car2car.cognito

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.amazonaws.mobileconnectors.cognitoidentityprovider.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler
import com.pear2pear.car2car.activities.HomeActivity
import com.pear2pear.car2car.R

import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(){

    //val userPool= CognitoUserPool(this, "us-east-1_jEqXt1Ptc",  "41adn49va2gr6madcqsak9s5tl", "1cqrl25k3gdoa9q7bb7f8d456tjljbf77el0sgq7005hm246ufha", Regions.US_EAST_1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val cognitosettings=
            CognitoSettings(this)  //Classe contente credenziali UserPool



        val authenticationHandle= object : AuthenticationHandler{ //Gestore di callback per il processo di autenticazione dell'utente.
            @SuppressLint("ResourceAsColor")
            override fun onFailure(exception: Exception?) {
                exception?.printStackTrace()
                println("login fallito")
                errorLogin.setTextColor(Color.RED)

            }

            override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
                println("login ok")
                errorLogin.setTextColor(Color.WHITE)
                val intent = Intent(this@LoginActivity, HomeActivity()::class.java)
                intent.putExtra("logged", true)
                intent.putExtra("user", username_edit.text.toString())
                this@LoginActivity.finish()
                startActivity(intent)
            }

            override fun getAuthenticationDetails( //Chiama il dev per ottenere le credenziali per un utente.
                authenticationContinuation: AuthenticationContinuation,
                userId: String?
            ) {

                val authenticationdetail= AuthenticationDetails(username_edit.text.toString(),password_edit.text.toString(),null)
                authenticationContinuation.setAuthenticationDetails(authenticationdetail)
                authenticationContinuation.continueTask()
            }

            override fun authenticationChallenge(continuation: ChallengeContinuation?) { //Challenge generica

                continuation!!.continueTask()

            }

            override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) { //MultifactorAuthentication

            }
        }

        sign_in_button.setOnClickListener(){


            val thisUser:CognitoUser=cognitosettings.getUserPool().getUser(username_edit.text.toString())
            thisUser.getSessionInBackground(authenticationHandle)



        }




    }















}
