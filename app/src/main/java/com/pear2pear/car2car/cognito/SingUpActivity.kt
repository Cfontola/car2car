package com.pear2pear.car2car.cognito

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.pear2pear.car2car.activities.CarInsertActivity
import com.pear2pear.car2car.R
import kotlinx.android.synthetic.main.activity_sing_up.*
import java.lang.Exception
import java.util.*

class SingUpActivity : AppCompatActivity() {

    private val insertAuto: CarInsertActivity =
        CarInsertActivity()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sing_up)

        Datebutton.setOnClickListener {
            showcalendar()
        }









        val signupCallback = object: SignUpHandler {
            override fun onSuccess(
                user: CognitoUser,
                signUpConfirmationState: Boolean,
                cognitoUserCodeDeliveryDetails: CognitoUserCodeDeliveryDetails?
            ) {

                val intent:Intent = Intent(this@SingUpActivity, ConfirmCodeActivity::class.java)
                intent.putExtra("Username", user.userId)
                this@SingUpActivity.finish()
                startActivity(intent)


            }

            override fun onFailure(exception: Exception?) {
                println("aaa"+exception)
                println("Registrazione fallita")
            }
        }

        signupobutton.setOnClickListener(){
            val userAttributes=CognitoUserAttributes()
            userAttributes.addAttribute("email", edit_mail.text.toString())
            userAttributes.addAttribute("phone_number",editphone.text.toString())
            userAttributes.addAttribute("birthdate",textDate.text.toString())
            userAttributes.addAttribute("name",editName.text.toString())
            userAttributes.addAttribute("custom:custom:Surname",editSurname.text.toString())
            userAttributes.addAttribute("custom:custom:CF",editCF.text.toString())
            userAttributes.addAttribute("custom:licenseID",editLicense.text.toString())
            val cognitosettings= CognitoSettings(this)
            cognitosettings.getUserPool().signUpInBackground(edit_user.text.toString(),editpsw.text.toString(),userAttributes,null,signupCallback)
        }
    }





    private fun OpenInsertAuto () {
        val intent = Intent(this, CarInsertActivity::class.java)
        val message = "eheh"

        intent.putExtra(EXTRA_MESSAGE, message)
        startActivity(intent)
    }

    private fun showcalendar(){
        val cal = Calendar.getInstance()
        val y = cal.get(Calendar.YEAR)
        val m = cal.get(Calendar.MONTH)
        val d = cal.get(Calendar.DAY_OF_MONTH)


        val datepickerdialog:DatePickerDialog = DatePickerDialog(this,
            AlertDialog.THEME_DEVICE_DEFAULT_DARK, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            var day=false
            if(dayOfMonth<10){
                day=true
            }

            // Display Selected date in textbox
            if(monthOfYear+1<10){
                if(day){
                    textDate.setText("""0$dayOfMonth/0${monthOfYear + 1}/$year""")
                }
                else{
                    textDate.setText("""$dayOfMonth/0${monthOfYear + 1}/$year""")
                }

            }
            else{
                if(day){
                    textDate.setText("""0$dayOfMonth/${monthOfYear + 1}/$year""")
                }
                else{
                    textDate.setText("""$dayOfMonth/${monthOfYear + 1}/$year""")
                }

            }

        }, y, m, d)

        datepickerdialog.show()
    }


}
