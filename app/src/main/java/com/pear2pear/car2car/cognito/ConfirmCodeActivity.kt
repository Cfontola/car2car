package com.pear2pear.car2car.cognito

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pear2pear.car2car.R
import com.pear2pear.car2car.presenter.ConfirmCodeHandlerPresenter
import kotlinx.android.synthetic.main.activity_confirm_code.*

class ConfirmCodeActivity : AppCompatActivity() {


    var code:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_code)
        var presenter=ConfirmCodeHandlerPresenter(this)
        if (intent.extras != null) {
            val bundle: Bundle = intent.extras

            //presenter.bind(this)



            confirm.setOnClickListener {
                Toast.makeText(this, "clicco", Toast.LENGTH_SHORT).show()
                  presenter.confirm(bundle.getString("Username"),edit_verify_code.text.toString())
            }
        }


    }

}
