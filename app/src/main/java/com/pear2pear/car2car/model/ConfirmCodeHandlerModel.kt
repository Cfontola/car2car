package com.pear2pear.car2car.model

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.pear2pear.car2car.cognito.CognitoSettings
import com.pear2pear.car2car.presenter.ConfirmCodeHandler

class ConfirmCodeHandlerModel(context: Context):ConfirmCodeHandler {

    var Context=context


    override fun confirmUser(handler: GenericHandler,user:String,code:String){
        CognitoSettings(Context).getUserPool().getUser(user).confirmSignUpInBackground(code,false,handler)
    }

}