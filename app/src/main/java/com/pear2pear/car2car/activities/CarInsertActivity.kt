package com.pear2pear.car2car.activities

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.graphics.Color
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_insert_auto.*
import java.lang.Exception
import java.util.*
import android.location.Address
import android.location.Geocoder
import android.widget.EditText
import android.widget.Toast
import com.pear2pear.car2car.restcalls.CarRestCalls
import com.pear2pear.car2car.R
import com.google.android.gms.maps.model.LatLng
import java.io.IOException

class CarInsertActivity : AppCompatActivity() {
    private var usertoinsert: String = ""



    fun searchLocation():LatLng {
        val locationSearch:EditText = findViewById<EditText>(R.id.position_editText)
        lateinit var location: String
        location = locationSearch.text.toString()
        var addressList: List<Address>? = null
        var latLng:LatLng= LatLng(0.0,0.0)

        if (location == null || location == "") {
            Toast.makeText(applicationContext,"provide location",Toast.LENGTH_SHORT).show()
        }
        else{
            val geoCoder = Geocoder(this)
            try {
                addressList = geoCoder.getFromLocationName(location, 1)

            } catch (e: IOException) {
                e.printStackTrace()
            }
            val address = addressList!![0]
            latLng = LatLng(address.latitude, address.longitude)
            Toast.makeText(applicationContext, address.latitude.toString() + " " + address.longitude, Toast.LENGTH_LONG).show()
        }

        return latLng

    }





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.usertoinsert = intent.getStringExtra("user")
        setContentView(R.layout.activity_insert_auto)

        bt_conferma.setOnClickListener {
            //val createdCar=Car(text_modello.text.toString(),text_marca.text.toString(),text_targa.text.toString(),"0-0",editFirstDate.text.toString(),usertoinsert)
            println("aaaa" +starttextView.text.toString())
            println("aaaa" +endtextView.text.toString())

            val createdCar= com.pear2pear.car2car.assets.Car(
                text_marca.text.toString(),
                text_modello.text.toString(),
                text_targa.text.toString(),
                "${searchLocation().latitude}-${searchLocation().longitude}",
                registerdatetextView.text.toString() + "T22:00:00.000+00:00",
                usertoinsert,
                starttextView.text.toString(),
                endtextView.text.toString(),
                false
            )
            val postexecutor= @SuppressLint("StaticFieldLeak")
            object:AsyncTask<com.pear2pear.car2car.assets.Car, Int, Boolean>(){
                override fun doInBackground(vararg p0: com.pear2pear.car2car.assets.Car): Boolean {
                    var out:Boolean=false
                    try {
                        out = CarRestCalls.insertCar(usertoinsert,p0[0])
                    }
                    catch(e: Exception){
                        e.printStackTrace()
                    }
                    return out
                }

                override fun onProgressUpdate(vararg values:Int?) {
                    super.onProgressUpdate(*values)
                }

                override fun onPostExecute(result: Boolean) {
                    //super.onPostExecute(result)
                    code.visibility= View.VISIBLE
                    if(result){
                        code.text= "Inserimento avvenuto con successo"
                        code.setTextColor(Color.GREEN)
                        this@CarInsertActivity.finish()
                    }
                    else{
                        code.text= "Inserimento fallito"
                        code.setTextColor(Color.RED)
                    }
                }
            }
            postexecutor.execute(createdCar)

            /*val intent = Intent(this@CarInsertActivity, myCarFragment()::class.java)
            intent.putExtra("marca", text_marca.text)
            intent.putExtra("modello", text_modello.text)
            intent.putExtra("targa", text_targa.text)
            startActivity(intent)*/

        }
        startdatebutton.setOnClickListener {
            showCalendar(1)
        }
        enddatebutton.setOnClickListener(){
            showCalendar(2)
        }
        registerdate.setOnClickListener(){
            showCalendar(3)
        }
        /*clockbutton.setOnClickListener {
            showClock()
        }*/
    }

    fun showCalendar(int:Int){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)+1
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, y, m, d ->
            if(int==1){starttextView.setText(String.format("%s-%s-%s",y,m+1,d))}
            if(int==2){endtextView.setText(String.format("%s-%s-%s",y,m+1,d))}
            if(int==3){registerdatetextView.setText(String.format("%s-%s-%s",y,m+1,d))}


        }, year, month, day)
        dpd.show()
    }

    /*fun showClock(){
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)



        val tpd = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { view, h, m ->

            editFirstClock.setText(h.toString() + " : " + m )

        }),hour,minute,false)

        tpd.show()
    }*/
}
