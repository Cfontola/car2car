package com.pear2pear.car2car.activities


import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.google.android.gms.location.*
import java.text.SimpleDateFormat
import java.util.*
import android.Manifest
import android.os.AsyncTask
import android.view.Menu
import android.view.MenuItem
import com.pear2pear.car2car.fragments.*
import com.pear2pear.car2car.cognito.CognitoSettings

import com.google.android.gms.maps.model.LatLng
import com.pear2pear.car2car.restcalls.CarRestCalls
import com.pear2pear.car2car.restcalls.RentRestCalls
import java.lang.Exception

import kotlin.collections.ArrayList
import com.pear2pear.car2car.R

class HomeActivity : AppCompatActivity() {
    /*--------------- fragments -------------------*/
    private val homeFragment: HomeFragment = HomeFragment()
    private val dashboardFragment: DashboardFragment = DashboardFragment()
    private val myCarFragment: myCarFragment = myCarFragment()
    private val notificationFragment: NotificationFragment = NotificationFragment()
    private val notLoggedFragment: NotLoggedFragment = NotLoggedFragment()
    /*------------------- aws cognito -------------------*/
    val cognitosettings = CognitoSettings(this)  //Classe contente credenziali UserPool
    private var isLogged : Boolean = false
    /*------------------- variabili location -------------------*/
    private var mFusedLocationProviderClient: FusedLocationProviderClient? = null
    private val INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = 1000
    lateinit var mLastLocation: Location
    internal lateinit var mLocationRequest: LocationRequest
    private val REQUEST_PERMISSION_LOCATION = 10

    /*-------------------- asyncTask ----------------------*/
    private class getCarTask:AsyncTask<String,Int,ArrayList<com.pear2pear.car2car.assets.Car>?>(){
        override fun doInBackground(vararg p0: String): ArrayList<com.pear2pear.car2car.assets.Car>? {
            var out:ArrayList<com.pear2pear.car2car.assets.Car>?
            try {
                val cal=Calendar.getInstance()
                cal.set(2019,5-1,1)
                out = CarRestCalls.getActiveByDateExcluding(cal.time,1.0,1.0,
                    User.current
                )
            }
            catch(e:Exception){
                out = null
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: ArrayList<com.pear2pear.car2car.assets.Car>?) {
            //super.onPostExecute(result)
            if (result!=null){
                CarFactory.insertCars(result)
            }
            else{
                //show error
            }
        }
    }

    private class getmyCarTask:AsyncTask<String,Int,ArrayList<com.pear2pear.car2car.assets.Car>?>(){
        override fun doInBackground(vararg p0: String): ArrayList<com.pear2pear.car2car.assets.Car>? {
            var out:ArrayList<com.pear2pear.car2car.assets.Car>?
            try {

                out = CarRestCalls.getCarsByName(User.current)

            }
            catch(e:Exception){
                out = null
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: ArrayList<com.pear2pear.car2car.assets.Car>?) {
            //super.onPostExecute(result)
            if (result!=null){
                CarFactory.insertmyCars(result)
            }
            else{
                //show error
            }
        }
    }

    private class getRentTask:AsyncTask<String,Int,ArrayList<com.pear2pear.car2car.assets.Rent>?>(){
        override fun doInBackground(vararg p0: String): ArrayList<com.pear2pear.car2car.assets.Rent>? {
            var out:ArrayList<com.pear2pear.car2car.assets.Rent>?
            println("vaDC "+ User.current)
            try {
                out = RentRestCalls.getMyRents(User.current)

            }
            catch(e:Exception){
                out = null
                println("vaDC "+e)
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: ArrayList<com.pear2pear.car2car.assets.Rent>?) {
            //super.onPostExecute(result)
            if (result!=null){
                println("vaDC")
                RentFactory.insertRent(result)
            }
            else{
                println("nonvaDC")
            }
        }
    }

    private class getReceiveRentTask:AsyncTask<String,Int,ArrayList<com.pear2pear.car2car.assets.Rent>?>(){
        override fun doInBackground(vararg p0: String): ArrayList<com.pear2pear.car2car.assets.Rent>? {
            var out:ArrayList<com.pear2pear.car2car.assets.Rent>?
            println("vaDC "+ User.current)
            try {
                out = RentRestCalls.getReceivedRents(User.current)
            }
            catch(e:Exception){
                out = null
                println("vaDC "+e)
                e.printStackTrace()
            }
            return out
        }

        override fun onProgressUpdate(vararg values:Int?) {
            super.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: ArrayList<com.pear2pear.car2car.assets.Rent>?) {
            //super.onPostExecute(result)
            if (result!=null){
                println("vaDC")
                RentFactory.insertreceiveRent(result)
            }
            else{
                println("nonvaDC")
            }
        }
    }

    object CurrentPosition{
        var Position:LatLng=LatLng(0.0,0.0)
        fun createPosition(
            Lat:Double,
            Long:Double
        ){
            Position =LatLng(Lat,Long)
        }
        fun returnPosition():LatLng{
            return Position
        }
    }

    /* GESTIONE GUI */
    private fun changeFragment (newFragment : Fragment) {
        if(!newFragment.isVisible) {
            val transaction = supportFragmentManager.beginTransaction()
            if(!isLogged && newFragment!=dashboardFragment )
            {
                transaction.replace(R.id.fragment_container, notLoggedFragment)
            }
            else {
                transaction.replace(R.id.fragment_container, newFragment)
            }
            transaction.commit()
        }
        return
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_top_bar,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id=item.itemId
        if(id==R.id.my_profile){
            val intent = Intent(this, MyAccountActivity::class.java)
            intent.putExtra("Username", User.current)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {
            item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                changeFragment(homeFragment)

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                changeFragment(dashboardFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_mycar -> {
                changeFragment(myCarFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                changeFragment(notificationFragment)
                return@OnNavigationItemSelectedListener true            }
        }
        false
    }

    object User{
        var current:String=""
    }

    /* LISTA AUTO */
    object CarFactory {
        var cars = ArrayList<com.pear2pear.car2car.assets.Car>()
        var mycars = ArrayList<com.pear2pear.car2car.assets.Car>()
        fun createCar(brand: String,
                      model: String,
                      plates: String,
                      position: String,
                      registrationDate : String,
                      username: String = "polo",
                      sharingPeriodStart: String = "01 - 01 - 2019",
                      sharingPeriodEnd: String = "01 - 12 - 2019",
                      using: Boolean = false,
                      carID: String) {
            println("home car create")
            val brumbrum = com.pear2pear.car2car.assets.Car(
                brand,
                model,
                plates,
                position,
                registrationDate,
                username,
                sharingPeriodStart,
                sharingPeriodEnd,
                using,
                carID
            )
            cars.add(brumbrum)
            return
        }
        fun insertCars(c:ArrayList<com.pear2pear.car2car.assets.Car>){
            cars.addAll(c)
            return
        }

        fun insertmyCars(c:ArrayList<com.pear2pear.car2car.assets.Car>){
            mycars.addAll(c)
            return
        }

        fun returnCar(index: Int): com.pear2pear.car2car.assets.Car {
            return cars[index]
        }

        fun returnCarByID(id:String): com.pear2pear.car2car.assets.Car {
            var index:Int=0
            for( i in 0 until cars.size){
                if (cars[i].carID==id){
                    index=i

                }
            }
            return cars[index]

        }
        fun returnCarInfo(index : Int) : String {
            println("home car info")
            val brum = cars[index]
            return brum.brand + " " + brum.model + " " + brum.plates + "\n" + brum.registrationDate
        }

        fun returnmyCarInfo(index : Int) : String {
            println("home car info")
            val brum = mycars[index]
            return brum.brand + " " + brum.model + " " + brum.plates + "\n" + brum.registrationDate
        }
    }

    /*--------- Lista Noleggi ---------*/
    object RentFactory{
        var rents=ArrayList<com.pear2pear.car2car.assets.Rent>()
        var receiverent=ArrayList<com.pear2pear.car2car.assets.Rent>()
        fun createRent(
            user:String,
            touser:String,
            date:Date,
            confirm:Boolean,
            isend:Boolean,
             idcar:String)
        {

            var RentCreate= com.pear2pear.car2car.assets.Rent(user, touser, date, confirm, isend, idcar)
            rents.add(RentCreate)
            return
        }

        fun insertRent(c:ArrayList<com.pear2pear.car2car.assets.Rent>){
            rents.addAll(c)
            return
        }

        fun insertreceiveRent(c:ArrayList<com.pear2pear.car2car.assets.Rent>){
            receiverent.addAll(c)
            return
        }

        fun returnRentInfo(index : Int) : String {
            println("rent info")
            val currentrent = rents[index]
            if(!currentrent.confirm) {
                return "Hai richiesto l'auto a ${currentrent.toUser}  \nIn data  ${currentrent.date.date}/${currentrent.date.month+1}/${currentrent.date.year+1900} \nIn attesa di conferma... "
            }
            else return currentrent.toUser+ " ha accettato la tua richiesta \nPer la data: ${currentrent.date.date}/${currentrent.date.month+1}/${currentrent.date.year+1900}\nIn attesa di scambio chiavi... "
        }

        fun returnreceiveRentInfo(index : Int) : String {
            println("rent info")
            val currentrent = receiverent[index]
            if(!currentrent.confirm) {
                return "Hai una richiesta da parte di ${currentrent.User}  \nIn data  ${currentrent.date.date}/${currentrent.date.month+1}/${currentrent.date.year+1900} \nConfermi??"
            }
            else return "Hai confermato la prenotazione di ${currentrent.User} \nPer la data: ${currentrent.date.date}/${currentrent.date.month + 1}/${currentrent.date.year + 1900} \nIn attesa di scambio chiavi..."
        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        buttoninhome.visibility= View.VISIBLE

        /* POSIZIONE */
        mLocationRequest = LocationRequest()
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps()
        }
        if (checkPermissionForLocation(this))
        {
            startLocationUpdates()
        }

        /* AUTO */
        CarFactory.cars.clear()
        println("Current"+ User.current)

        if(CarFactory.cars.size == 0 ) println("home - non va un ca")

        for( i in 0 until CarFactory.cars.size) {
            println("home - car $i di ${CarFactory.cars.size} ${CarFactory.returnCarInfo(
                i
            )}")
        }

        if( intent.extras != null ) {
            val bundle: Bundle = intent.extras
            if(cognitosettings.getUserPool().currentUser.userId!=""){
                isLogged = bundle.getBoolean("logged")
                User.current = bundle.getString("user")
                CarFactory.cars.clear()
                CarFactory.mycars.clear()
                RentFactory.rents.clear()
                RentFactory.receiverent.clear()
                getRentTask().execute(User.current)
                getReceiveRentTask().execute(User.current)
                getCarTask().execute(User.current)
                getmyCarTask().execute(User.current)

                buttoninhome.visibility= View.VISIBLE
                changeFragment(homeFragment)
            }

        }else{
            buttoninhome.visibility= View.GONE
            User.current=""
            isLogged=false
            changeFragment(homeFragment)
            CarFactory.cars.clear()
            CarFactory.mycars.clear()
            RentFactory.rents.clear()
            RentFactory.receiverent.clear()
            getCarTask().execute()
            navigation.setSelectedItemId(R.id.navigation_dashboard)
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        buttoninhome.setOnClickListener(){
            if(isLogged) {
                val thisUser: CognitoUser = cognitosettings.getUserPool().getUser(User.current)
                thisUser.signOut()
                isLogged = false
                User.current =""
                CarFactory.cars.clear()
                CarFactory.mycars.clear()
                RentFactory.rents.clear()
                RentFactory.receiverent.clear()
                getCarTask()
                    .execute()

                buttoninhome.visibility = View.GONE
                changeFragment(dashboardFragment)
                navigation.setSelectedItemId(R.id.navigation_dashboard)

            }
        }
    }

    override fun onStop() {
        super.onStop()
        stoplocationUpdates()
    }

    /*------ metodi location -----*/
    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id ->
                startActivityForResult(
                    Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    , 11)
            }
            .setNegativeButton("No") { dialog, id ->
                dialog.cancel()
                finish()
            }
        val alert: AlertDialog = builder.create()
        alert.show()
    }

    protected fun startLocationUpdates() {
        // Create the location request to start receiving updates
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.setInterval(INTERVAL)
        mLocationRequest!!.setFastestInterval(FASTEST_INTERVAL)

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        return
        }
        mFusedLocationProviderClient!!.requestLocationUpdates(mLocationRequest, mLocationCallback,
            Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // do work here
            locationResult.lastLocation
            onLocationChanged(locationResult.lastLocation)
        }
    }

    fun onLocationChanged(location: Location) {
        // New location has now been determined
        mLastLocation = location
        val date: Date = Calendar.getInstance().time
        val sdf = SimpleDateFormat("hh:mm:ss a")
        println("Updated at : " + sdf.format(date))
        CurrentPosition.Position =(LatLng(mLastLocation.latitude,mLastLocation.longitude))
        // You can now create a LatLng Object for use with maps
    }

    private fun stoplocationUpdates() {
        mFusedLocationProviderClient!!.removeLocationUpdates(mLocationCallback)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates()
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun checkPermissionForLocation(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                // Show the permission request
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_PERMISSION_LOCATION)
                false
            }
        } else {
            true
        }
    }
}