package com.pear2pear.car2car.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_my_account.*
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler
import com.amazonaws.mobileconnectors.cognitoidentityprovider.tokens.CognitoAccessToken
import com.pear2pear.car2car.cognito.CognitoSettings
import com.pear2pear.car2car.restcalls.GameRestCalls
import com.pear2pear.car2car.R
import com.pear2pear.car2car.cognito.LoginActivity
import kotlinx.android.synthetic.main.activity_confirm_code.*


class MyAccountActivity : AppCompatActivity() {

    val cognitosettings=
        CognitoSettings(this)  //Classe contente credenziali UserPool


    val deletehandler = object: GenericHandler {
        override fun onSuccess() {
            text_alert.text="Utente confermato"

            this@MyAccountActivity.finish()
        }

        override fun onFailure(exception: java.lang.Exception?) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)
        val User=cognitosettings.getUserPool().getUser(HomeActivity.User.current)
            val handler = object : GetDetailsHandler {
                override fun onSuccess(list: CognitoUserDetails) {

                    profileUserNameTextView.text=User.userId
                    profileNameTextView.text=list.attributes.attributes["name"]
                    profileSurnameTextView.text=list.attributes.attributes["custom:custom:Surname"]
                    profilePhoneNumberTextView.text=list.attributes.attributes["phone_number"]
                    profileEmailTextView.text=list.attributes.attributes["email"]
                    profileBirthDateTextView.text=list.attributes.attributes["birthdate"]
                    profileCFTextView.text=list.attributes.attributes["custom:custom:CF"]
                    profilelicenseTextView.text=list.attributes.attributes["custom:licenseID"]


                }

                override fun onFailure(exception: Exception) {
                    println("nonfunzia")

                }
            }
        User.getDetailsInBackground(handler)






        /*------------------------------------------------------*/


        //Prelevo punti esperienza da User
        val gametask= @SuppressLint("StaticFieldLeak")
        object:AsyncTask<String,Int, com.pear2pear.car2car.assets.User>(){
            override fun doInBackground(vararg p0: String): com.pear2pear.car2car.assets.User? {
                var u: com.pear2pear.car2car.assets.User?=null
                try{
                    u = GameRestCalls.getUserGame(p0[0])
                }
                catch (e:Exception){
                    e.printStackTrace()
                }
                return u
            }

            override fun onProgressUpdate(vararg values: Int?) {
                super.onProgressUpdate(*values)
            }

            override fun onPostExecute(result: com.pear2pear.car2car.assets.User?) {
                super.onPostExecute(result)
                if(result!=null){
                    LevelTextView.text="Level "+result.getLevel().toString()
                    LevelprogressBar.progress=result.getcurrentExp()
                    LevelPercentageView.text=result.getcurrentExp().toString()+"/100 pe"
                }
                else{
                    println("nonvaunca")
                }
            }
        }



        gametask.execute(HomeActivity.User.current) //e se venisse distrutta la HomeActivity?
        //LevelTextView.text="Level "+User(HomeActivity.User.current).getLevel().toString()
        //LevelprogressBar.progress=User(HomeActivity.User.current).getcurrentExp()
        //LevelPercentageView.text=User(HomeActivity.User.current).getcurrentExp().toString()+"/100 pe"

        //fine
        delete_account.setOnClickListener(){
            HomeActivity.User.current="delete"
            User.deleteUserInBackground(deletehandler)
            this.finish()
        }
       



    }


    override fun onResume() {
        super.onResume()
        val User=cognitosettings.getUserPool().getUser(HomeActivity.User.current)
        val handler = object : GetDetailsHandler {
            override fun onSuccess(list: CognitoUserDetails) {

                profileUserNameTextView.text=User.userId
                profileNameTextView.text=list.attributes.attributes["name"]
                profileSurnameTextView.text=list.attributes.attributes["custom:custom:Surname"]
                profilePhoneNumberTextView.text=list.attributes.attributes["phone_number"]
                profileEmailTextView.text=list.attributes.attributes["email"]
                profileBirthDateTextView.text=list.attributes.attributes["birthdate"]
                profileCFTextView.text=list.attributes.attributes["custom:custom:CF"]
                profilelicenseTextView.text=list.attributes.attributes["custom:licenseID"]


            }

            override fun onFailure(exception: Exception) {
                println("nonfunzia")

            }
        }
        User.getDetailsInBackground(handler)
    }


}
