package com.pear2pear.car2car.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.pear2pear.car2car.restcalls.RentRestCalls
import kotlinx.android.synthetic.main.activity_car_view.*
import kotlinx.android.synthetic.main.activity_insert_auto.*
import java.lang.Exception
import java.util.*
import com.pear2pear.car2car.R
import com.pear2pear.car2car.assets.Rent

class CarViewActivity : AppCompatActivity() {


    var Date=""
    var Brand=""
    var Model=""
    var Targa=""
    var startSh=""
    var endSh=""
    var username=""
    var id=""

    var year=0
    var month=0
    var day=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_view)
        if( intent.extras != null ) {
            val bundle: Bundle = intent.extras
            Date = bundle.getString("Date")
            Brand = bundle.getString("Brand")
            Model = bundle.getString("Model")
            Targa = bundle.getString("Targa")
            startSh = bundle.getString("StartSh")
            endSh = bundle.getString("EndSh")
            username = bundle.getString("Username")
            // id=bundle.getString("Id")
            DateCarView.text = Date
            TargaView.text = Targa
            NameAutoView.text = Brand+" "+Model
            textStartSh.text = "Disponibilità: $startSh - $endSh"

        }
        println("//88"+startSh)
        calendarView.minDate=Date(startSh.split("-")[0].toInt()-1900,startSh.split("-")[1].toInt()-1,startSh.split("-")[2].toInt()).time
        calendarView.maxDate=Date(endSh.split("-")[0].toInt()-1900,endSh.split("-")[1].toInt()-1,endSh.split("-")[2].toInt()).time

        calendarView.setOnDateChangeListener { calendarView, y, m, d  ->
            year = y
            month = m
            day = d
        }

        rentrequestbtn.setOnClickListener(){
            var rent= Rent(
                HomeActivity.User.current,
                username,
                Date(year - 1900, month, day),
                false,
                false,
                id
            )
            val postexecutor = @SuppressLint("StaticFieldLeak")
            object: AsyncTask<com.pear2pear.car2car.assets.Rent, Int, Boolean>(){
                override fun doInBackground(vararg p0: com.pear2pear.car2car.assets.Rent): Boolean {
                    var out:Boolean=false
                    try {
                        out = RentRestCalls.insertRentRequest(p0[0])
                    }
                    catch(e: Exception){
                        println("dcdc"+e)
                        e.printStackTrace()
                    }
                    return out
                }

                override fun onProgressUpdate(vararg values:Int?) {
                    super.onProgressUpdate(*values)
                }

                override fun onPostExecute(result: Boolean) {
                    //super.onPostExecute(result)
                    code.visibility= View.VISIBLE
                    if(result){
                        Toast.makeText(this@CarViewActivity, "Richiesta inoltrata", Toast.LENGTH_SHORT).show()

                    }
                    else{
                        //fallimento

                        Toast.makeText(this@CarViewActivity, "Richiesta fallita", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            postexecutor.execute(rent)
        }

    }

}
