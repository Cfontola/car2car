package com.pear2pear.car2car.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pear2pear.car2car.R
import kotlinx.android.synthetic.main.activity_car_view.*

class CarEditActivity : AppCompatActivity() {

    lateinit var date : String
    lateinit var brand : String
    lateinit var model : String
    lateinit var targa : String
    lateinit var startSh : String
    lateinit var endSh : String
    lateinit var username : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_edit)
        if( intent.extras != null ) {
            val bundle: Bundle = intent.extras
            date = bundle.getString("Date")
            brand = bundle.getString("Brand")
            model = bundle.getString("Model")
            targa = bundle.getString("Targa")
            startSh = bundle.getString("StartSh")
            endSh = bundle.getString("EndSh")
            username = bundle.getString("Username")
            DateCarView.text = date
            TargaView.text = targa
            NameAutoView.text = "$brand $model"
            //textStartSh.text="Disponibilità: "+startSh+" - "+endSh
        }
        println("grafica - schermata auto $brand $model di $username")
    }
}
